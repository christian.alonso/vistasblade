<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('user');
    }
    public function welcome(){
        return view('welcome2');
    }
    public function about(){
        return view('about');
    }
    public function future(){
        return view('future');
    }
}
